const mqtt = require('mqtt');
const config = require('./table.config.js');

export default function getServerAddress(staticAddress, callback) {
    // Mock address
    if (staticAddress) {
        console.log('Target session server is statically set to %s', config.StaticServerUri);
        callback(config.StaticServerUri)
    } else {
        // Real Mqtt 
        const client = mqtt.connect(config.MqttBroker, {connectionTimeout: 2000});

        client.on('error', function () {
            console.log("Could not resolve server host address. retrying in 1000 ms");
        });
        
        client.on('reconnect', function () {
           console.log('Reconnecting to mqtt broker...'); 
        });
        
        client.on('connect', function() {
            client.subscribe(config.ServerUriTopic);
        });

        client.on('message', function(topic, message) {
            // got server adress 
            console.log('Session server running @ %s', message.toString());
            // disconnect from broker and clean up
            client.end();
            // set session server address
            callback(message.toString());
        });
    }
}