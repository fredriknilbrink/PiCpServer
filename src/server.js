/* eslint no-console: 0 */
const path = require('path');
const express = require('express');

import webpack from 'webpack';
import webpackMiddleware from 'webpack-dev-middleware';
import webpackHotMiddleware from 'webpack-hot-middleware';
import config from './webpack.config.js';
import ProcessMonitor from './process-monitor.js';

const isDeveloping = process.env.NODE_ENV !== 'production';
const port = isDeveloping ? 3000 : process.env.PORT;
const app = express();

if (isDeveloping) {
    const compiler = webpack(config);
    const middleware = webpackMiddleware(compiler, {
        publicPath: config.output.publicPath,
        contentBase: 'src',
        stats: {
            colors: true,
            hash: false,
            timings: true,
            chunks: false,
            chunkModules: false,
            modules: false
        }
    });

    app.use(middleware);
    app.use(webpackHotMiddleware(compiler));
    app.get('/', function response(req, res) {
        res.write(middleware.fileSystem.readFileSync(path.join(__dirname, 'dist/index.html')));
        res.end();
    });
} else {
    app.use(express.static(__dirname + '/dist'));
    app.get('/', function response(req, res) {
        res.sendFile(path.join(__dirname, 'dist/index.html'));
    });
}

// process runner
const processMonitor = new ProcessMonitor();

if(require('os').platform() == 'linux' && require('os').arch() == 'arm') {
    //Hopefully running on a raspberry pi, otherwise this will totally not work :)
    const gpio = require('rpi-gpio');
    const pin = 7;
    const delay = 500;
    let count = 0;
    let max = 10;

    gpio.setup(pin, gpio.DIR_OUT, on);

    function on() {
        if (count >= max) {
            console.log('Start up ok, now ready to start tracking');
            setInterval(trackerState, delay * 2);
            return;
        }

        setTimeout(function() {
            gpio.write(pin, 1, off);
            count += 1;
        }, delay);
    }
    function off() {
        setTimeout(function() {
            gpio.write(pin, 0, on);
        }, delay);
    }
    // run forever to visualize state of tracker process
    function trackerState() {
        const trackerState = processMonitor.isRunning() 
        gpio.write(pin, trackerState, function(err) {
            if (err) 
                throw err;
            //console.log(`tracker is ${trackerState ? 'running': 'stopped'}`);
        });
    }
}

// REST like interface without the stateless part :).
// Should probably use POST/DELETE to get the semantics correct 
// but i don't really care just now.
// =============================================================================
var router = express.Router();             

// middleware to use for all requests
router.use(function(req, res, next) {
    // do logging
    console.log('Got a call');
    next(); // handle request
});
router.route('/start-tracker').get(function(req, res) {
    processMonitor.start();
    res.json({ message: 'Trying to start tracker' });
});
router.route('/stop-tracker').get(function(req, res) {
    processMonitor.stop();
    res.json({ message: 'Trying to stop tracker' });
});
router.route('/shutdown').get(function(req, res) {
    res.json({ message: 'Shutting down, ok to power off in 10 seconds' });
    shutdown();
});
router.route('/reboot').get(function(req, res) {
    res.json({ message: 'Rebooting, online in a minute. Please refresh your browser'});
    reboot();
});
// register routes
// all of our routes will be prefixed with /api
app.use('/api', router);


// Start HttpServer
const server = app.listen(port, '0.0.0.0', function onStart(err) {
    if (err) {
        console.log(err);
    }
    console.info('==> 🌎 Listening on port %s. Open up http://localhost:%s/ in your browser.', port, port);
});

// WebSocket api
const io = require('socket.io').listen(server);
io.sockets.on('connection', function(socket) {
    processMonitor.onProcessInfo(function(trackerOutput) {
        socket.emit('tracker-output', trackerOutput);
    });
    socket.on('start-tracker', function(params) {
        // request to start tracker service
        processMonitor.start();
    });
    socket.on('stop-tracker', function(params) {
        // request to stop tracker service
        processMonitor.stop();
    });
    socket.on('shutdown', function (params) {
        shutdown();
    });
    socket.on('reboot', function (params) {
        reboot();
    });
});


// General helpers
function shutdown() {
    const os = require('os').platform()
    const spawn = require('child_process').spawn;
    if (os == 'win32')
        spawn('shutdown', ['-s', '-f']);
    else
        spawn('shutdown', ['-h', 'now']); // Unix style os:s
}

function reboot() {
    const os = require('os').platform()
    const spawn = require('child_process').spawn;
    if (os == 'win32')
        spawn('shutdown', ['-r', '-f']);
    else
        spawn('shutdown', ['-r', 'now']); // Unix style os:s
}