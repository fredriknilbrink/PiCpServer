// Publish state and address to topics
const mqtt = require('mqtt');
const ip = require('ip');
const config = require('./table.config.js');

// Publishes a JSON formated string containing the
// current ip(usally private) of the table and a
// timestamp set to the current date.
export default function publishTableInfo(isTrackerRunningCb, startCb, stopCb) {
    const client = mqtt.connect(config.MqttBroker, {
        connectionTimeout: 2000
    });
    
    var intervalObject = null;

    client.on('connect', function() {
        client.subscribe(config.TrackerRunTopic);

        // only ever set one interval for publish
        if (intervalObject == null) {
            intervalObject = setInterval(function() {
                const topic = config.TableStatusTopic + '/' + config.TableId;
                client.publish(topic, JSON.stringify({
                    IP_ADDRESS: ip.address(),
                    TIME_STAMP: Date.now(),
                    TRACKER_STATE: isTrackerRunningCb()
                }));
            }, config.TablePublishInterval)
        }
    });

    client.on('message', function(topic, message) {
        const trackerRunState = message.toString();
        if (trackerRunState == 'on') {
            startCb();
        } else if (trackerRunState == 'off') {
            stopCb();
        }
    });
}