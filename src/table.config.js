
// configuration
module.exports = {
    TableId: 0,
    
    // Markers belonging to this table
    MarkerIds: [0, 1, 2, 3, 4, 5],
    
    // MQTT broker instance to use
    MqttBroker: 'mqtt://146.185.136.237',
    
    // Use static address and don't lookup using MQTT
    UseStaticServerUri: false,
    
    // Address to use if UseStaticServerUri == true
    StaticServerUri: '192.168.2.16',
    
    // string ex: '192.168.1.10' or 'www.cp2015.nl'
    ServerUriTopic: 'cp2015/server-ip',
    
    // Json {IP_ADDRESS: <String>, TIME_STAMP: <Number>, TRACKER_STATE: <String>}
    TableStatusTopic: 'cp2015/info',
    
    // Either 'on' or 'off' plain string
    TrackerRunTopic: 'cp2015/run',
    
    // Interval at witch the table updates its state on the broker
    TablePublishInterval: 10000,
    
    // shell command line to start tracker service
    //TrackerRunCommand: 'dummy.exe -c=table_configuration.yml'
    TrackerRunCommand: '/opt/tracker/PiCpTracker -c=/opt/tracker/table_configuration.yml'
};