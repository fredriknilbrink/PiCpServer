import React from 'react';

import Colors from 'material-ui/lib/styles/colors';
import RaisedButton from 'material-ui/lib/raised-button';
import getMuiTheme from 'material-ui/lib/styles/getMuiTheme';
import themeDecorator from 'material-ui/lib/styles/theme-decorator';
import Toolbar from 'material-ui/lib/toolbar/toolbar';
import ToolbarGroup from 'material-ui/lib/toolbar/toolbar-group';
import ToolbarSeparator from 'material-ui/lib/toolbar/toolbar-separator';
import ToolbarTitle from 'material-ui/lib/toolbar/toolbar-title';
import Badge from 'material-ui/lib/badge';
import io from 'socket.io-client'
import styles from './App.css';
import config from '../table.config'

const muiTheme = getMuiTheme({
  accent1Color: Colors.deepOrange500,
});

// channel to relay real-time updates
const socket = io();

class App extends React.Component {
    constructor(props, context) {
        super(props, context);
        this.state = {
            tableId: config.TableId,
            console: [''],
            running: false
        };
    }

    componentDidMount() {
        socket.on('tracker-output', (stdout) => {
            // State should be treated immutable
            // Create new Array from previus and push stdout 
            let newConsole = this.state.console.slice();
            // Limit buffered lines for performance resons
            if(newConsole.length > 40)
                newConsole.pop();
            newConsole.unshift(stdout);
            // Set the newly created state
            this.setState({ console: newConsole })
        });
    }
    
    tryStartTracker() {
        console.log('starting tracker');
        if(socket.connected) {
            socket.emit('start-tracker');
            this.setState({ running: true });    
        }
    }
    
    tryStopTracker() {
        console.log('stopping tracker');
        if(socket.connected) {
            socket.emit('stop-tracker');
            this.setState({ running: false });    
        }
    }
	
	tryShutdownTracker() {
        console.log('stopping tracker');
        if(socket.connected) {
            socket.emit('shutdown');
            this.setState({ running: false });    
        }
    }
	
	tryRebootTracker() {
        console.log('rebooting tracker');
        if(socket.connected) {
            socket.emit('reboot');
            this.setState({ running: false });    
        }
    }

    render() {
        return (
			<div>
				<div>
					<Toolbar>
						<ToolbarGroup>
							<ToolbarTitle text='Tracker Server Interface'/>
						</ToolbarGroup>
						<ToolbarGroup float="right">
							<Badge
							badgeContent={this.state.tableId}
							secondary={true}
							badgeStyle={{ top: 12, right: -3 }}
							>
							Table ID 
						</Badge>
						</ToolbarGroup>										
					</Toolbar>
				</div>
				<div>
					<Toolbar>
						<ToolbarGroup>
							<ToolbarTitle text='Tracker Control'/>
						</ToolbarGroup>
						<ToolbarGroup float="right">
							<ToolbarSeparator />
							<RaisedButton 
								onMouseUp={e => this.tryShutdownTracker(e.target)}
								label="Shutdown"
								primary={true} 
							/>
							<RaisedButton 
								onMouseUp={e => this.tryRebootTracker(e.target)}
								label="Restart" 
								secondary={true} 
							/>
						</ToolbarGroup>
					</Toolbar>
				</div>
				<div>
					<Toolbar>
						<ToolbarGroup>
							<ToolbarTitle text='Tracker Monitor'/>
						</ToolbarGroup>
						<ToolbarGroup float="right">
							<ToolbarSeparator />
							<RaisedButton 
								onMouseUp={e => this.tryStartTracker(e.target)}
								label="Start"
								primary={true} 
							/>
							<RaisedButton 
								onMouseUp={e => this.tryStopTracker(e.target)}
								label="Stop" 
								secondary={true} 
							/>							
						</ToolbarGroup>
					</Toolbar>
					
				</div>
				<ConsoleList consoleOutput={this.state.console} />
			</div>
        );
    }
}

// Component to imitate standard command shell
const ConsoleList = ({consoleOutput}) => {
    return (
        <ul className={styles.consoleBox}>
            {consoleOutput.map((line, index) => {
                return <li key={index}>{'> ' + line}</li>;
            })}
        </ul>
    );
}

export default themeDecorator(muiTheme)(App);