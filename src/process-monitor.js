import hostResolver from './host-resolver.js'
import publishTableInfo from './state-publisher.js'
const exec = require('child_process').exec;
const spawn = require('child_process').spawn;
const config = require('./table.config.js');

export default class ProcessMonitor {

    constructor() {
        this.child = null;
        this.os = require('os').platform();
        this.uri = null;
        // try to get session host address from mqtt broker
        hostResolver(false, (address) => {
            // address of session host
            this.uri = address;
            // set shell command used to spawn tracker
            this.shellcmd =
                config.TrackerRunCommand +
                ' -a=' + address +
                ' -t=' + config.TableId;
            // Then start publishing state of this Table forever
            publishTableInfo(
                this.isRunning.bind(this),
                this.start.bind(this),
                this.stop.bind(this)
            );
        });
    }

    _isUriSet() {
        if (this.uri === null) {
            if (typeof(this.callback) === 'function') {
                this.callback('Error: no address set for destination');
            }
            console.log('Error: no address set for destination')
            return false;
        }
        return true;
    }

    isRunning() {
        return this.child ? true : false;
    }

    onProcessInfo(callback) {
        this.callback = callback;
    }

    start() {
        if (!this._isUriSet())
            return;

        if (this.child === null) {
            // start new instance
            /*this.child = exec(this.shellcmd, (error, stdout, stderr) => {
                if (error !== null) {
                    if (typeof(this.callback) === 'function') {
                        this.callback(`${error}`);
                    }
                    console.log(`${error}`);
                    // error handling & exit
                }
                // clear child reference after termination
                this.child = null;
            });*/
            const subStrings = this.shellcmd.split(" ");
            this.child = spawn(subStrings[0], [subStrings[1], subStrings[2], subStrings[3]]);

            // callback for stdout 
            this.child.stdout.on('data', (data) => {
                if (typeof(this.callback) === 'function') {
                    this.callback(`${data}`);
                }
                console.log(`${data} from pid ${this.child.pid}`);
            });

            // callback on process end
            this.child.on('close', (code, signal) => {
                if (signal)
                    console.log(`child process terminated due to receipt of signal ${signal}`);
                else
                    console.log(`child process terminated with return code ${code}`)
                    // clear child reference after termination
                this.child = null;
            });

            //callback on error
            this.child.on('error', (error) => {
                if (typeof(this.callback) === 'function') {
                    this.callback(`${error}`);
                }
                console.log(`Failed to start child process. Reason: ${error}`);
            });
        } else {
            // already started
            if (typeof(this.callback) === 'function') {
                this.callback(`process already running pid: ${this.child.pid}`);
            }
            console.log(`process already running pid: ${this.child.pid}`);
        }
    }

    stop() {
        if (!this._isUriSet())
            return;

        if (this.child != null) {
            if (typeof(this.callback) === 'function') {
                this.callback(`terminate process with pid ${this.child.pid} on ${this.os}`);
            }

            if (this.os == 'win32') {
                // Workaround for windows, do the brutal thing
                // as it doesn't implement posix signals
                spawn("taskkill", ["/pid", this.child.pid, '/t', '/f']);
            } else {
                // On Linux we can use the standard SIGINT to exit
                // the tracker gracefully
                // Kill by name as pid refers to shell and not actual tracker process
                spawn("pkill", ["PiCpTracker"]);
                //this.child.kill();
            }
            console.log(`terminate process with pid ${this.child.pid} on ${this.os}`)
        } else {
            if (typeof(this.callback) === 'function') {
                this.callback('nothing to stop');
            }
            console.log('nothing to stop')
        }
    }
};