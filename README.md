## cp-tracker-monitor
helper script to manage state of tracker service
based on boilerplate from https://github.com/christianalfoni/webpack-express-boilerplate

## Install and develop
`git clone this`

1. cd PiCpServer
2. npm install
3. npm run debug
4. navigate to http://localhost:?? 

## Production build
1. npm run build
2. sudo reboot
3. Open a browser at http://<pi_local_ip>:3000 

## Add to init.d
Uses a startup script from <https://github.com/chovy/node-startup>
to install follow the descriptions on the page, then to test simply write:
    /etc/init.d/node-app start
other options like stop, restart and status also works. To make the
service start on boot type the following:
    update-rc.d node-app defaults
test that everything works by rebooting and checking the state 
of the service by typing:
    sudo reboot
    /etc/init.d/node-app status
and check if it says it's running.

if for some reason the Pi hangs or loses power unexpectantly the
script will not start the service due to a pid file that got lost.
In that case run this command to clean up:
    /etc/init.d/node-app start --force
this will ignore the previous pid file and everything will work as intended.