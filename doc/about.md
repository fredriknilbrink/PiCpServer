# About

## Purpose

Local NodeJs instance running as a daemon on each Table. Presumably on Raspberry Pi V2/v3 hardware.
Two main responsibilities - First, start and stop tracker instance and direct
output to correct endpoint. Second, test, debug and general trouble shooting   

## Exposed API:s

### HTTP
Example usage: "GET http://[server-address]:[port]/api/start-tracker" to start
or "GET http://[server-address]:[port]/api/stop-tracker" to stop

### WS
Used for live updates if accessing app at http://[server-address]:[port] 

### MQTT
Start tracker: publish 'on' to topic /changing/perspective/v-1.0.0/run
Stop tracker: publish 'off' to topic /changing/perspective/v-1.0.0/run
topic is configurable in table.config.js


### Camera calibration routine (should not be necessary)
1. Record 60 seconds video from command line using proper frame size
   * raspivid -o calibration.h264 -w 512 -h 512 -t 60000
2. Convert raw H264 to something playable
   * sudo apt-get update
   * sudo apt-get install -y gpac
   * MP4Box -fps 30 -add calibration.h264 calibration.mp4
3. Run calibration app on a proper windows machine and copy
   resulting calibration file to install directory
   * Calibration.exe -w=5 -h=7 --sl=600 --ml=500 -d=5 -v=calibration.mp4 --sc=true calibration.yml